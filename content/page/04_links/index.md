---
title: "Links"
slug: "links"
readingTime: false

links:
  - title: GitLab (main)
    website: https://gitlab.com/f-matano44
    image: https://cdn.cdnlogo.com/logos/g/8/gitlab.svg

  - title: GitHub (sub)
    website: https://github.com/f-matano44
    image: https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png

  - title: AtCoder
    website: https://atcoder.jp/users/f_matano44

  - title: Qiita (記事は本サイトに移行済)
    website: https://qiita.com/F_matano44

menu:
  main:
    weight: -60
    params:
      icon: link

license: false

comments: false
---
