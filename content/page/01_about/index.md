---
title: About me
slug: "aboutme"
readingTime: false

menu:
  main:
    weight: -90
    params:
      icon: user

license: false
---

## Who am I?

- 㑨野文義 (俣野文義)
- MATANO, Fumiyoshi
- 明治大学 [\[森勢研究室\]](https://www.isc.meiji.ac.jp/~mmorise/lab/) M2


## Research Theme

* 普通の声と感情が込められた声は何が違うの？


### 論文 (Peer-reviewed paper)

* 俣野文義, 小口純矢, 森勢将雅,
``嫌悪感情を意図して発話された日本語演技音声の音響特徴量分析と話者間比較,''
日本音響学会誌, Vol. 81, No. 1, pp. 64--72, 2025.
[\[doi: 10.20697/jasj.81.1_64\]](https://doi.org/10.20697/jasj.81.1_64)

* F. Matano, Y. Tagusari, T. Horibe, J. Koguchi, M. Morise,
``Taut-MUSHRA: A MUSHRA-based method without hidden reference and anchors for relative sound quality evaluation,''
Acoustical Science and Technology, Vol. 46, Issue 1, pp. 100--102, 2025.
[\[PDF\]](./46_e24.34.pdf)
[\[doi: 10.1250/ast.e24.34\]](https://www.doi.org/10.1250/ast.e24.34)


### 学会等 (Non-peer-reviewed)

#### 2024

* Fumiyoshi MATANO,
``Statistic Speech Analysis of Disgust Emotion in Japanese Play-acted Speech,''
WASP-Meiji university joint Workshop 2024, 2024-10-07.

* 俣野文義, 小口純矢, 森勢将雅,
``音声コーパス構築のための仮定を追加した発話区間検出法の提案と基礎評価,''
日本音響学会 第 152 回 (2024 年秋季) 研究発表会, pp. 1161--1162, 2024-09-04.

* 俣野文義, 小口純矢, 森勢将雅,
``改良したMUSHRA法に対する符号化音声を用いた検証,''
日本音響学会 第 152 回 (2024 年秋季) 研究発表会, pp. 867--868, 2024-09-05.

* 蟹江世莉奈, 俣野文義, 小口純矢, 森勢将雅,
``プロ声優が発話した様々な発話スタイルの統計解析,''
日本音響学会 第 152 回 (2024 年秋季) 研究発表会, pp. 1103--1104, 2024-09-05.

* 俣野文義, 森勢将雅,
``jMARS Recorder: コーパス朗読に特化した音声収録アプリの制作と検討,''
日本音響学会 第 151 回 (2024 年春季) 研究発表会, pp. 1061--1062, 2024-03-07.

#### 2023

* 俣野文義, 森勢将雅,
``jMARS Recorder: 音声コーパス収録用レコーダーの試作,''
第 26 回日本音響学会 関⻄支部 若手研究者交流研究発表会, 2023-12-09.

* 俣野文義, 森勢将雅,
``日本語嫌悪感情音声の音響特徴量解析,''
日本音響学会 第 150 回 (2023 年秋季) 研究発表会, pp. 1381--1382, 2023-09-28.

* 俣野文義, 松井淑恵, 森勢将雅,
``DNN 音声合成による嫌悪感情の表現と基礎評価,''
研究報告音声言語情報処理 (SLP), Vol. 2023-SLP-147, No. 50, pp. 1--4, 2023-06-24.


## Qualification(s)

上位階級（及び上位互換）の検定・資格がある場合は，合格・取得した中から最上位の資格のみ記載しています。

- 2024-11: 1Z0-818-JPN Java SE Bronze
- 2023-11: 第三級アマチュア無線技士
- 2022-04: 普通自動車運転免許
- 2021-10: TOEIC L&R 625 (IP 試験)
- 2021-03: IT パスポート試験
- 2020-11: 実用数学技能検定 準 1 級
- 2020-11: ディジタル技術検定 2 級 情報部門
- 2019-11: ディジタル技術検定 2 級 制御部門

## Award

- 2020-01: [ディジタル技術検定二級制御部門 優良賞](https://web.archive.org/web/20201022123805/https://www.toyota-ct.ac.jp/information/18263/)

<!--
- 2015-10: [第 68 回全日本合唱コンクール全国大会 銀賞](https://web.archive.org/web/20161021155257/https://jcanet.or.jp/event/concour/kako/con68.htm)
-->

## Other(s)

- 2019-03: [First Robotics Competition: Team 7631 Cool Guy](https://web.archive.org/web/20200918193339/https://www.toyota-ct.ac.jp/information/12269/)
