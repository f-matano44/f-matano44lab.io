---
title: "Search"
layout: "search"
slug: "search"
readingTime: false

outputs:
  - html
  - json
menu:
  main:
    weight: -50
    params:
      icon: search
---
