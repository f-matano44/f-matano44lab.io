---
title: "Contact"
slug: "contact"
readingTime: false

menu:
  main:
    weight: -30
    params:
      icon: messages

license: false
---

* メールアドレス：matano.fumiyoshi.fx \(あ\) tut.jp
* 大学アドレス（2025 年３月失効）：cs232038 \(あ\) meiji.ac.jp
