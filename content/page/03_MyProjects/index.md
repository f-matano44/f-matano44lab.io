---
title: "My projects"
slug: "myprojects"
readingTime: false

links:
  - title: pySATEN
    description: 発話区間推定のための Python ライブラリ
    website: https://pypi.org/project/pysaten/

  - title: jMARS Recorder
    description: 音声コーパス収録用レコーダー
    website: https://gitlab.com/f-matano44/jmars-recorder

  - title: jFloatWavIO
    description: wav ファイルを浮動小数点配列として読み込むための Java ライブラリ
    website: https://gitlab.com/f-matano44/jfloatwavio

  - title: JA-WORLD
    description: WORLD ボコーダの Java 移植
    website: https://gitlab.com/f-matano44/world-for-java


menu:
  main:
    weight: -70
    params:
      icon: link

license: false
---
