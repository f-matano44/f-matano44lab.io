---
date: 2023-06-08
lastmod: 2025-03-05T18:20:00+0900
title: "VSCode で MATLAB を使う"
categories: ["Programming"]
tags: ["MATLAB", "VSCode"]
---

## 上位互換

なんか気づかぬうちに MATLAB 拡張機能が進化しまくってた（v1.2.0 以上？）っぽいので，今からやる方は以下の記事に従うことを推奨します。
言い訳をすると最近 MATLAB 使ってなかったから気づかんかった……。

* https://qiita.com/iiTAi/items/ec663c789a84a12a3760


## 作者の環境

一応私の使用環境を書いておきますが, 同じようなことをすれば (Unix 系なら) どの環境でも動くと思います (Windows はわからん).

* macOS Monterey 12.6.5
* MATLAB R2022a
* VSCode 1.78.2


## やること

### MATLAB の公式拡張機能をインストール

> * 名前: MATLAB
> * ID: MathWorks.language-matlab
> * 説明: Edit MATLAB code with syntax highlighting, linting, navigation support, and more
> * バージョン: 1.1.0
> * パブリッシャー: MathWorks
> * VS Marketplace リンク: https://marketplace.visualstudio.com/items?itemName=MathWorks.language-matlab


### パスの設定

VSCode のターミナルから MATLAB を起動できるようにします.
パスの設定, とは書きましたが厳密には実行ファイルがあるフォルダへのパスではなく, 実行ファイルそのものを指すエイリアスを設定します.
なぜかというと今回は CLI のみが必要で GUI はいらないからですね.
これには `-nodesktop` オプションを使います.

ということで次のコマンドを, 使ってるシェルに応じて `~/.bashrc`　とか `~/.zshrc` とかに設定してください.
私は fish ユーザーなので ` ~/.config/fish/config.fish` に設定します.


```bash
alias matlab '/Applications/MATLAB_R2022a.app/bin/matlab -nodesktop'
```

設定したら `source` で設定ファイルの再読み込みをかけてください.
これで OK.
ターミナルで `matlab` コマンドを実行すると CLI モードで MATLAB が立ち上がります.


## HelloWorld を実行してみるとこんな感じ

![MATLAB in VSCode](matlab_in_vscode.png)

変数名の表示とかはできませんが, 簡単なスクリプトを書いて実行するくらいなら全然これでも問題なく使えます. 個人の感想ですが.
