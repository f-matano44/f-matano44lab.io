---
date: 2023-12-16
lastmod: 2025-03-05T19:40:00+0900
title: "Tacotron 2 で ずんだもん に喋らせる"
categories: ["Programming"]
tags: ["Python", "PyTorch", "TensorFlow", "Tacotron 2"]
---


## 豊橋技科大☆最高！！

これは [\[裏TUT Advent Calendar 2023 / 17 日目\]](https://adventar.org/calendars/9090) の記事です．
ちなみに私は卒業生なので 2023 年現在，完ッ全な部外者となってしまっています．
あとこの記事に技科大要素がない気がしてきました．
どうしよ，ままええか．

[\[つっけんどん\]](https://dictionary.goo.ne.jp/word/%E7%AA%81%E3%81%A3%E6%85%B3%E8%B2%AA/)

昨日はどどくんの記事でしたね．
相変わらず誤家庭の逸般人をされていらっしゃる……。


## 目的

ずんだもん に好きなセリフを喋らせたい！！！！！
え，VOICEVOX を使えって？野暮だなあ，私がずんだもんのママになるってのが良いんじゃないですか．


## 方法

[\[NVIDIA/Tacotron 2\]](https://github.com/NVIDIA/tacotron2) + [\[HiFi-GAN\]](https://github.com/jik876/hifi-gan) を使用します．
具体的には LJSpeech で pre-train された Tacotron 2 のモデルに [\[ずんだもんマルチモーダルデータベース©SSS\]](https://zunko.jp/multimodal_dev/login.php) で fine-tuning します．
Neural vocoder は HiFi-GAN の公式リポジトリで公開されているモデルをそのまま使います．
本来であれば fine-tuning するべきですが，まあ声を出すだけならその必要もないかなって．


## 今回の環境

* Ubuntu 22.04 (WSL 2)
* [\[miniconda\]](https://docs.conda.io/projects/miniconda/en/latest/)
    * Python 3.10.13
    * [\[torch 1.13.1\]](https://pytorch.org/get-started/previous-versions/#v1131) [\(cuda 11.6\)](https://anaconda.org/nvidia/cuda-toolkit)


## 環境構築

### Windows 側の設定

WSL2 を有効にして Ubuntu 22.04 をインストール．
あと Windows 上の GPU ドライバが古すぎる場合は更新してください（古すぎなければ大丈夫だと思う）．

### 環境の選定方法について

再現性のために，今回使用した環境の選定方法について此処に記す……
まず [\[nvidia/acotron2\]]((https://github.com/NVIDIA/tacotron2)) のページより，1 系（機械工学系ではない）のtensorflow と PyTorch 1.0 が必要であることが分かります．
しかし何方も壊滅的に古く，[\[特に tensorflow 1.x は現在サポートされている Python では対応されていないため\]](https://pypi.org/project/tensorflow/1.15.5/)，そのままのバージョンを使用することは今回諦めます．
その代わりと言っては何ですが，今回使用するライブラリにおいては今後の事を考え，なるべく新しいバージョンを使い，かつコードの変更は最小限となるようなバランスを目指しました．
まず tensorflow 1.x はどうしようもないため 2.x を使用し，PyTorch は 1.x の中で可能な限り最新のものを使用します．
その他のライブラリも試行錯誤しつつバージョンを決定しました．
あと，この記事では conda を使用するので，気に食わない方は適当なものに脳内 `sed` してください．

まず使用する CUDA のバージョンを決定します．
`nvidia-smi` コマンドを実行してください．
色々出てきますが，一番重要なのは `CUDA Version: x.y` です．
すでに CUDA のバージョン表記あるやんけ！となりますが [\[実はこれインストール可能な CUDA の最新バージョン名が表記されているだけ\]](https://qiita.com/ketaro-m/items/4de2bd3101bcb6a6b668#nvidia%E3%83%89%E3%83%A9%E3%82%A4%E3%83%90) です．
なので今回の場合は `CUDA <= 12.3` であれば良いということになります．

```
+----...---------------------------+
| NVI...    CUDA Version: 12.3     |
|----...----+----------------------+
```

さて以上の情報をまとめましょう．

* Tensorflow は 2 系（電気・電子情報工学系ではない）
* PyTorch は 1 系の中で最新のもの
* CUDA は 12.3 以下

以上の条件で環境を構築していきます．
Python のバージョンは [\[PyTorch にサポートされており\]](https://pytorch.org/blog/deprecation-cuda-python-support/)，[\[サポート切れじゃない範囲\]](https://devguide.python.org/versions/#versions) で良い感じに決定して下さい．

### CUDA + PyTorch 環境の作成

適当にインストールします．
が，インストールするだけだと [\[一部のライブラリが PyTorch から見えないっぽい\]](https://qiita.com/cacaoMath/items/811146342946cdde5b83) んで最後に環境変数を追加しています．

```
conda create -n tac2 python=3.10
conda activate tac2
conda install -c "nvidia/label/cuda-11.6.2" cuda-toolkit
conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia
echo "export LD_LIBRARY_PATH=/usr/lib/wsl/lib:$LD_LIBRARY_PATH" >> ~/.bashrc
source ~/.bashrc && conda activate tac2
```

### tacotron 2 環境の作成

先に述べた通り `requirements.txt`を使うと，まともに動かないので以下のコマンドを使用してください．
ちなみにこれは絶妙なバランスで成り立っています，改善点があったとしても勘弁してください．
もう何もしたくないんです．

```sh
conda install -c conda-forge nvidia-apex
git clone https://github.com/NVIDIA/tacotron2.git
cd tacotron2
git submodule init; git submodule update
conda install -c conda-forge numpy=1.23 matplotlib=3.8 scipy=1.11 tensorflow=2.11 inflect=7.0 librosa=0.10 unidecode=1.3 pillow=9.4 jupyterlab pydub=0.25
```

次に `google-perftools` とかいうのを入れます．
必須ではないですがこれを入れると [\[メモリの確保速度を改善してくれるらしい\]](https://note.com/npaka/n/n152b77fbc192#vce7Q) です．
効果のほどはというと，僕が実行した環境だと目に見えて早くなるレベルでしたのでインスコをおすすめします．
`/path/to/libtcmalloc_minimal.so.4` の部分はインスコされた場所に応じて書き換えてください．
場所は`dpkg -L libtcmalloc-minimal4` で出てきます．
露骨に一番それっぽいのがあるのでそれをコピってください．

```
sudo apt install -y google-perftools
echo "export LD_PRELOAD=/path/to/libtcmalloc_minimal.so.4" >> ~/.bashrc
source ~/.bashrc && conda activate tac2
```


## 学習

だから僕は Python アンチなんだよ．

### の前にコード修正

#### Tensorflow

まず Tensorflow のバージョン変わっちゃった問題を解決します．
これについては Tensorflow サイドも気づいているようで，以降コマンドの `tf_upgrade_v2` というコマンドが用意されています．
一度ディレクトリを指定するととは勝手に変換してくれます．

```
cd ../
tf_upgrade_v2 --intree tacotron2/ --outtree tacotron2_v2/ --reportfile report.txt
```

ですがこれだけでは変換しきれないため，一部（`hparams.py`）手動で調整してあげる必要があります．
これは私が書くと長いので [\[このリンク\]](https://github-com.translate.goog/NVIDIA/tacotron2/issues/278?_x_tr_sl=auto&_x_tr_tl=ja&_x_tr_hl=ja) を参照してください．
issue が既に建てられていました．

#### LibRosa

stft を行う関数がバージョンの違いで動かないのでこれも手動で修正します．
`stft.py` の 67 行目を変更してあげる必要があります．
[\[参考リンク\]](https://librosa.org/doc/main/generated/librosa.util.pad_center.html)

```
  File "/path/to/tacotron2_v2/stft.py", line 67, in __init__
    fft_window = pad_center(fft_window, filter_length)
TypeError: pad_center() takes 1 positional argument but 2 were given
```

```diff
- fft_window = pad_center(fft_window, filter_length)
+ fft_window = pad_center(fft_window, size=filter_length)
```

次に，学習につかうメルスペクトログラムの生成コードを治してあげる必要があります．
`layers.py` の 50~51 行目を修正してください．
[\[参考リンク\]](https://shoma-knot.github.io/posts/2023-12-hifigan/)

```
  File "/path/to/tacotron2_v2/layers.py", line 50, in __init__
    mel_basis = librosa_mel_fn(
TypeError: mel() takes 0 positional arguments but 5 were given
```

```diff
mel_basis = librosa_mel_fn(
- sampling_rate, filter_length, n_mel_channels, mel_fmin, mel_fmax)
+ sr=sampling_rate, n_fft=filter_length, n_mels=n_mel_channels, fmin=mel_fmin, fmax=mel_fmax)
```

これでやっとこさ最低限動くようになります．はぁ・・・（疲労）

### Train

#### 参考資料の皆様

* https://qiita.com/mana_b/items/a0dd5f76a30d781d0fa3
* https://shirowanisan.com/entry/2020/12/08/215504
* https://note.com/npaka/n/n2a91c3ca9f34#o453V


#### 台本の作成

まず，今回使うずんだもん音声は [\[ROHAN コーパス\]](https://github.com/mmorise/rohan4600) を読んだものなので，これをベースに train と validation の transcript ファイルを作成します．
ROHAN は読み仮名が全て割り振られているので，そこから音素列を取得します．
今回は `pyopenjtalk` を使用しました．
また，このファイルを読み込めるように，`hparams.py` の `training_files` と `validation_files` をこれらのファイルのパスに変更してください．
あとついでに今回は日本語音声を学習させるので `text_cleaners` も `basic_cleaners` に変更します．

* transcript_train.txt

```
path/to/ROHAN4600_0001.wav|geguanwakonotokorotashaomikudasushi,chottoodokasuka.
path/to/ROHAN4600_0002.wav|guerutsooniwa,sakenarawokkatosupurittsuaokonomimasuna.
path/to/ROHAN4600_0003.wav|myechisuwafuwa,tsugihaginihararetagamuteepuo,baribaritohippegasu.
......
```

* transcript_val.txt

```
path/to/ROHAN4600_4401.wav|sumaatofonnikaikaerukotookimemashita.
path/to/ROHAN4600_4402.wav|tsorunnohodainoshoomeigamuzukashisugite,zooosurawaitekimashita.
path/to/ROHAN4600_4403.wav|warewarebonjinni,wisukiinoajinadowakarimasen.
......
```

#### 音声ファイルの設置

さっき設置した台本ファイルは，見ていただくとわかりますが `path/to/wavfile.wav|台本の音素列` みたいな形式になっているので，適当に設置したあと `path/to/wavfile.wav` を良い感じに書き換えます．

#### ハイパラの設定

なんか良い感じに頑張れ．そのへんは学習に使う PC による．

#### pre-train モデルのダウンロード

https://drive.google.com/file/d/1c5ZTuT7J08wLUoVZ2KkUs_VdZuJ86ZqA/view

#### 学習開始

そろそろ書くの疲れてきた．

```shell
python -m multiproc train.py --output_directory=outdir --log_directory=logdir -c tacotron2_statedict.pt --warm_start
```

### inference

学習が回りきったとします．
推論しましょう．
今回は `inference.ipynb` のコードを使いますが，neural vocoder に HiFi GAN を使用するので一度 npy 形式で書き出す必要があります．
適当に `Decode text input and plot results` の `mel_outputs_postnet` を `np.save` してください．
まあ多分 numpy 配列を直接 HiFi GAN に渡しても実行はできると思います，多分（やってないのでわからない）．

HiFI GAN の方は得に言うこともないです．
`inference_e2e.py` の中身を書き換えて，以下のリンクから neural vocoder のモデルをダウンロードして推論コマンドを打ち込みます．

https://drive.google.com/drive/folders/1-eEYTB5Av9jNql0WGBlRoi-WH2J7bp5Y

```sh
python inference_e2e.py --checkpoint_file model/generator_v1
```

先行研究的にはこれでイケるはずです．
本来ならこれをベースにもう少し研究チックな記事を書こうと思ったんですけど，時間的な問題で間に合いませんでした……ので，ここまでの内容で学習を回していきます．
これについては結果次第でまた別の記事にするかも？
まあ，この記事はこの記事で「最新の環境で NVIDIA/tacotron2 を動作させた」ことに新規性があるので許してください．
動作させることは研究の第一歩です．


### 結果は・・・？（ドキドキ）

* 無音区間があったので一部トリミングしています
* 追記：Safari では再生出来ない可能性があります

{{< audio "post/20231217_zundamon/test_generated_e2e.mp3" >}}

#### 音声について

生成した音声については，学習素材となったマルチモーダルデータベース利用の取り決め 第3条（遵守事項）４に「本件データを使った成果物を商用目的で利用したい場合は事前に乙に連絡し承認を得ること。」という項目があるため，CC BY 4.0 が適用できない可能性があります。


## まとめ

寝言みたいで可愛いね，可愛いので合成は成功です．
成功です！！！！！！

明日は庭崎さんが「TRPGはいいぞと言う話と、TRPGはやめとけという話（仮）」という話について語ってくれるらしいです．
TRPG でいうとニコニコアワード 合成音声ソフト ゲーム実況以外部門 のニコライさんの動画がめちゃ面白いのでオススメなので良ければどうぞ．
現在 4 位の妙楽さんもそっちの人です．
