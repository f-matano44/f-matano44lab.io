---
date: 2022-08-25
lastmod: 2025-03-03T14:35:00+0900
title: Steam の Windows ゲーム を Linux で動かす時にフレームレートが不安定になる時がある問題を解決するかもしれない起動オプション
categories: ["GNU/Linux"]
tags: ["Wine", "Proton"]
---

タイトルが長い


[\[この記事は Qiita からのお引越しです.\]](https://web.archive.org/web/20220825114415/https://qiita.com/F_matano44/items/cbc41d92d17c0e38c559)


## どうするの
以下を起動オプションにコピペ。

```
gamemoderun %command%
```

## どういう理屈なの
詳しくはわからん。
ただ [\[ArchWiki (閲覧日 2022-08-24)\]](https://wiki.archlinux.jp/index.php/Gamemode) によると "ゲームがホスト OS やゲームプロセスに最適化のセットを一時的に適用することを要求できるようにするもの" らしい。
多分デフォルトだと設定とかが微妙なんだろうな、と思った（小学生並の感想）。


## 実際効果はどの程度ある？
筆者の環境だと Monster Hunter World でアステラを歩き回った時のフレームレートが 50-70 fps 辺りで不安定だったものが 70 fps 付近に張り付くという改善が見られたので、個人的にはかなり効果があると思う。
もしフレームレートが不安定なゲームがあった場合には試す価値はあるかもしれない……。
