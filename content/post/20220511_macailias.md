---
date: 2022-05-11
title: おっちょこちょい Mac ユーザー向けの alias
categories: ["Macintosh"]
tags: ["shell"]
---

* [\[この記事は Qiita からのお引越しです.\]](https://web.archive.org/web/20220511044948/https://qiita.com/F_matano44/items/8eb147c997962c96ab26)
* 変更履歴を記録していなかったため公開時の版と異なる可能性があります


## 幸せになりたい
以下の alias をお使いのシェルに対応する rc ファイル (zshrc, bashrc) に書くとほんの少しだけ幸せになれます、多分。

```bash
# 日本語入力が起動した状態でも端末を閉じられる
alias えぃｔ='exit'

# Python2 は使えない
alias python='python3'
alias pip='pip3'

# Linux のつもりで実行すると良くないコマンドの皆様
alias reboot='shutdown -r now'
alias halt='shutdown -h now'
```

それだけ
