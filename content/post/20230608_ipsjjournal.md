---
date: 2023-06-08
title: "情報処理学会 (研究報告) の日本語テンプレートを Overleaf で使う"
tags: ["LaTeX"]
featured_image: ''
disable_share: false
---

私は音学シンポジウム 2023 の原稿作成で使いました.

## 注意
この記事は研究報告 (tech-jsample.tex) のテンプレートを Overleaf で使用する方法をまとめたものです. ジャーナル論文の場合は既に Overleaf テンプレートが存在するのでそちらを使われたほうが良いかもしれません. 私は使ったことないので (というかジャーナル論文を書いたことがない) そちらを使われる場合は何か良い感じに調べてください.  
https://ja.overleaf.com/latex/templates/qing-bao-chu-li-xue-hui-lun-wen-zhi-tenpureto-2018nian-11yue-6ri-ban-yori/xmqtnxffwtvt

## 注意 2
Overleaf のアカウントは既に所持しているものとします.

## テンプレートの入手
### ダウンロード
まずここからテンプレートをダウンロードします. 
https://www.ipsj.or.jp/journal/submit/style.html

### 解凍 & 必要ファイルの抽出
ファイルを解凍すると, `SJIS` と `UTF8` の 2 種類が出てきます. 今回は UTF8 のみを使用しますので, `UTF8/` ディレクトリに入ってください. ここから必要なファイルを取り出します. どこか適当な場所に新しいディレクトリを作成し, 次のファイルを移動 (コピー) してください.

1. bibsample.bib
2. ipsj.cls
3. ipsjtech.sty
4. ipsjunsrt.bst (ipsjsort.bst でも O.K.)
5. tech-jsample.tex (ファイルの移動後 main.tex に名前変更)

## アップロード
### プロジェクトの作成 & 初期設定
Overleaf を開き, New Project から Blank Project を作成してください. その後メニューからコンパイラを latex に変更し,ます. これだけだと日本語をコンパイルできないので, 新規ファイルから `latexmkrc` を作成し, そこに以下のサイトから `pLaTeX + dvipdfmx を使用する場合` のコードををコピペしてください.

https://doratex.hatenablog.jp/entry/20180503/1525338512#pLaTeX--dvipdfmx-%E3%82%92%E4%BD%BF%E7%94%A8%E3%81%99%E3%82%8B%E5%A0%B4%E5%90%88

### ファイルのアップロード
アップロードから, 先程別フォルダに隔離したファイルをアップロードしてください.

## コンパイルして完成!
お疲れ様でした.
