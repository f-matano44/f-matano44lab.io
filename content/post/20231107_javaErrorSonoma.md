---
date: 2023-11-07T00:00:00+09:00
title: "【解決方法】Java で Secure coding is not enabled for restorable state! という警告が出て正常に動かない"
tags: ["Java", "macOS"]
featured_image: ''
disable_share: false
---

## 環境
* OS: macOS 14.1
* gradle: 8.3
* JDK: 17.0.8.1

## エラーが出た
Swing を使用した Java GUI アプリを実行したときに

WARNING: Secure coding is not enabled for restorable state! Enable secure coding by implementing NSApplicationDelegate.applicationSupportsSecureRestorableState: and returning YES.

みたいな感じの警告が出てマイクにアクセスできなくなった. どうしよう.

## 解決方法
全部更新. 私の場合は brew で管理してるので以下のコマンドで更新した.

```
brew update
brew upgrade
```

## 新環境
* OS: macOS 14.1
* gradle: 8.4
* JDK: 17.0.9

## 結果
治った. が, なんか警告は出続けている.

WARNING: Secure coding is automatically enabled for restorable state! However, not on all supported macOS versions of this application. Opt-in to secure coding explicitly by implementing NSApplicationDelegate.applicationSupportsSecureRestorableState:.

まあ動いたので良いか. 詳細については, まあそのうち詳しい人がわかりやすいブログを書いてくれると信じて......
