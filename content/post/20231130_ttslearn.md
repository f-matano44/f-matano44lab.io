---
date: 2023-11-30
title: "とりあえず Windows 上で ttslearn を動作させてみる with WSL"
tags: ["WSL", "Windows", "Ubuntu", "ttslearn"]
featured_image: ''
disable_share: false
---


## 前提知識
この記事では Linux でのコマンド操作がある程度必要になってくる．そのため，ある程度のコマンドは覚えていたほうが良いかもしれない．ただしレベルとしては高度な物は使用しないため `ls`, `cd` コマンドの意味をそれぞれ調べるまでもなく回答できる様であれば十分だと思われる．わからない場合は適宜 gg ってもらえば良い．


## WSL の起動まで
* Microsoft store から Windows Subsystem for Linux をインストール
* Microsoft store から Ubuntu 22.04.x をインストール
    * ここで `WslRegisterDistribution failed with error: 0x80070424` というエラーが出て起動しない場合がある
    * スタートメニュー → `Windows の機能の有効化または無効化` を検索 → 開く
    * `仮想マシン プラットフォーム` にチェックを入れる → `OK` (必要ならここで再起動)
    * 実は過去にインスコ経験があったとかで起動できなかった場合はこことかが役に立つかも
        * https://www.rk-k.com/archives/6873
* 初回起動時に ユーザー名 と パスワードを設定する
    * 起動時の説明文にも書いてあるとおり，これは Windows の ユーザー名，パスワード と一致させる必要はない
    * username の入力は良いとして，パスワードは入力しても表示されないので注意（入力はちゃんとされている）

設定が上手くいくとこんな感じになる

```
Installing, this may take a few minutes...
Please create a default UNIX user account. The username does not need to match your Windows username.
For more information visit: https://aka.ms/wslusers
Enter new UNIX username: username
New password:
Retype new password:
passwd: password updated successfully
この操作を正しく終了しました。
Installation successful!

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

Welcome to Ubuntu 22.04.2 LTS (GNU/Linux 5.15.133.1-microsoft-standard-WSL2 x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage


This message is shown once a day. To disable it please create the
/home/username/.hushlogin file.
username@DESKTOP-xxxxxxx:~$ _
```


## WSL 起動後の初期設定

* パッケージリスト，インスコ済みアプリを更新する．もしパスワードの入力を求められた場合は先程設定したパスワードを入力すれば OK．
    * \<\< 入力
    ```
    sudo apt update && sudo apt upgrade -y
    ```
    * \>\> 出力
    ```
    Get:1 http://security.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
    Get:2 http://security.ubuntu.com/ubuntu jammy-security/main amd64 Packages [1002 kB]
    Get:3 http://security.ubuntu.com/ubuntu jammy-security/main Translation-en [193 kB]
    Get:4 http://security.ubuntu.com/ubuntu jammy-security/main amd64 c-n-f Metadata [11.4 kB]
    ......
    ```
* このページを参考にしつつ，日本語化用のコマンドを実行．このときの注意として，`$` はコマンドに含める必要はない．例えば `$ sudo apt ...` みたいなコマンドがあった場合は `sudo apt ...` をコピペする．
    * https://qiita.com/tomonori_hioki/items/e76eed5af63cf98f0495
    * この記事は Ubuntu 20.04 向けの記事だが，22.04 でも動作確認済み（2022-11-30 現在）


## Python 環境の作成

今回の記事では必要最小限の構成で話を勧めていくので「仮想環境の構築方法が気に食わん」みたいな人は適宜単語を `sed` しつつ読み進めて貰えば良い．← これが何言ってるのかわからない人はそのまま読み勧めてもらえば OK.

* 最低限必要なアプリをインストール
    * \<\< 入力
    ```
    sudo apt install -y \
        build-essential python3 \
        python-is-python3 \
        python3-venv python3-pip
    ```
    * \>\> 出力
    ```
    パッケージリストを読み込んでいます... 完了
    依存関係ツリーを作成しています... 完了
    状態情報を読み取っています... 完了
    ......
    ```
* ttslearn リポジトリのクローン
    * \<\< 入力
    ```
    git clone https://github.com/r9y9/ttslearn.git
    ```
    * \>\> 出力
    ```
    Cloning into 'ttslearn'...
    remote: Enumerating objects: 2204, done.
    remote: Counting objects: 100% (160/160), done.
    remote: Compressing objects: 100% (54/54), done.
    remote: Total 2204 (delta 118), reused 122 (delta 105), pack-reused 2044
    Receiving objects: 100% (2204/2204), 51.76 MiB | 6.91 MiB/s, done.
    Resolving deltas: 100% (884/884), done.
    ```
* 先程の作業により，ホームフォルダに移動上に `ttslearn` フォルダができるはずなので，まずは存在を確認
    * \<\< 入力
    ```
    ls | grep ttslearn
    ```
    * \>\> 出力
    ```
    ttslearn
    ```
* ttslearn を使用するために ttslearn フォルダに移動に入る
    * \<\< 入力
    ```
    cd ttslearn
    ```
    * \>\> 出力
    ```
    （成功した場合は出力なし）
    ```
* ttslearn 用の仮想環境を作成する
    * Python では様々なライブラリをインストールすることによりプログラムを実行できるようにする．しかしプログラムによって必要なライブラリは異なるため，それぞれのプログラム用に専用のライブラリのセットを組む必要がある．これが仮想環境．今回は教本で使用する pip のために venv を用いる．
    * 仮想環境の作成
        * \<\< 入力
        ```
        python -m venv .venv
        ```
        * \>\> 出力
        ```
        （成功した場合は出力なし）
        ```
* 仮想環境を起動し，必要なライブラリをインストールする．
    * \<\< 入力
    ```
    source .venv/bin/activate
    ```
    * \>\> 出力
    ```
    （成功した場合は出力なし．その代わりユーザー名の前に（.venv）の文字が付く）
    ```
    * \<\< 入力
    ```
    pip install ttslearn jupyterlab librosa
    ```
    * \>\> 出力
    ```
    Collecting ttslearn
    Downloading ttslearn-0.2.2.tar.gz (295 kB)
        ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 295.2/295.2 KB 3.4 MB/s eta 0:00:00
    Installing build dependencies ... done
    Getting requirements to build wheel ... done
    Preparing metadata (pyproject.toml) ... done
    ......
    ```
* 最後に Jupyter-lab を起動
    * \<\< 入力
    ```
    jupyter-lab
    ```
    * \>\> 出力
    ```
    [I 2023-11-30 19:00:48.109 ServerApp] Package jupyterlab took 0.0000s to import
    [I 2023-11-30 19:00:48.120 ServerApp] Package jupyter_lsp took 0.0102s to import
    [W 2023-11-30 19:00:48.120 ServerApp] A `_jupyter_server_extension_points` function was not found in jupyter_lsp. Instead, a `_jupyter_server_extension_paths` function was found and will be used for now. This function name will be deprecated in future releases of Jupyter Server.
    ......
    ```
    * 起動し終わったら URL が出てくるので，それを Windows 上のブラウザにコピペすれば OK.
　
* これで環境構築は終了．


## Python 環境の起動

* 上の Python 環境を起動するにはどうすればよいのか？という話
* まずは ttslearn フォルダに移動
    * \<\< 入力
    ```
    cd ttslearn
    ```
    * \>\> 出力
    ```
    （成功した場合は出力なし）
    ```
* 次に venv の起動
    * \<\< 入力
    ```
    python -m venv .venv
    ```
    * \>\> 出力
    ```
    （成功した場合は出力なし）
    ```
* 最後に Jupyter-lab を起動
    * \<\< 入力
    ```
    jupyter-lab
    ```
    * \>\> 出力
    ```
    [I 2023-11-30 19:00:48.109 ServerApp] Package jupyterlab took 0.0000s to import
    [I 2023-11-30 19:00:48.120 ServerApp] Package jupyter_lsp took 0.0102s to import
    [W 2023-11-30 19:00:48.120 ServerApp] A `_jupyter_server_extension_points` function was not found in jupyter_lsp. Instead, a `_jupyter_server_extension_paths` function was found and will be used for now. This function name will be deprecated in future releases of Jupyter Server.
    ......
    ```
    * 起動し終わったら URL が出てくるので，それを Windows 上のブラウザにコピペすれば OK.

あとは ttslearn の本通りに読み進めれば OK だと思われる．おそらく途中で色々足りないライブラリが出てくると思われるので，それはそのタイミングで `pip install ......` してあげれば良い．がんばれ．
