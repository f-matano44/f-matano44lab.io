---
date: 2025-03-04
lastmod: 2025-03-05T17:54:00+0900
title: "Linux Mint を USB ブートしたいのに起動できない"
categories: ["GNU/Linux"]
tags: ["Linux_Mint"]
license: "[ARTICLE: CC BY 4.0]"
---

## 発生した問題

Linux Mint をインストールしている PC に LMDE をインストールしようとしたら

```
Verifiying shim SBAT data failed: Security Policy Violation
Something has gone seriously wrong: SBAT self-check failed: Security Policy Violation
```

と表示されて，インストールメディアが Boot できない。


## 解決方法

1. 起動直後に（モデルにもよるが）F2 とか DEL とかを連打して，UEFI (BIOS) 設定を起動
1. Boot 設定の項目から **[\[Secure boot\]](https://wa3.i-3-i.info/word16972.html) を無効化**
1. 外部メディアから Linux を起動 → OS をインストール
1. 外部メディアを引っこ抜いてインストールした OS を起動
1. 内蔵パッケージを更新
1. PC を再起動 → 再度 UEFI 設定を起動
1. Secure Boot を有効化


## より詳細なお話

### 原因？
まずこのエラーで検索すると「Windows Update が原因」とする記事がいくつか出てくる。

* https://qiita.com/yutorimatsugami/items/fd015175258487825e98
* https://owl-blog.net/verifiying-shim-sbat-data-failed/
* https://zenn.dev/sushichaaaan/scraps/b4d5857b980d45

ただ今回のケースでは Linux Mint をインストールしていた PC に LMDE をインストールしようとしているため，Windows は関係ないはず。
ただ [\[空の HDD でも起動に失敗した\]](https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q10303396910) という話も出てきたので，よく分からんがダメなときはダメなんでしょうね。
どちらにせよ，原因は UEFI の Secure Boot 周りってことは確か。


### 解決方法？
「Secure Boot が原因だからこれをオフにすれば動く」っていうのはまあそう。
ただ Secure Boot がオフなのはちと怖いので何とか解決したい。
そのためには何にせよまず起動しなきゃならんので，ここは一旦諦めて Secure Boot を切った状態で起動（ここで起動出来なかったら多分別の問題）。
そのままインストール。

で，いれた OS を起動すると（これはどのディストロもそうですけど）内蔵パッケージの更新が来てるはずなのでこれを更新します。
すると何か良く分かりませんが，Secure Boot をオンにしても正常に起動するようになります（更新しないと起動できないまま）。
たぶん更新で [\[shim の証明書 (shim-signed)\]](https://packages.debian.org/ja/sid/shim-signed) が更新されるのが理由じゃないかとは思っていますが，詳細な検証は面倒なので追走者に任せます。

それだけ
