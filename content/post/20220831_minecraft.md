---
date: 2022-08-31
lastmod: 2025-03-03T14:35:00+0900
title: Linux mint Ulyssa で Minecraft が動かない
categories: ["GNU/Linux"]
tags: ["Linux_Mint", "minecraft"]
---

[\[この記事は Qiita からのお引越しです.\]](https://web.archive.org/web/20220831053515/https://qiita.com/F_matano44/items/8596ccda444dae2cd26a)


## 動かない
から JRE をコネコネして動かしましょうねっていう記事を書こうとしたのですが、原因を探していたらまさかの [\[Linux mint のリポジトリにある Official Minecraft Launcher が古すぎる\]](http://packages.linuxmint.com/search.php?release=any&section=any&keyword=minecraft)、というものでした、そんな……。
しかもこれがかなりタチ悪く、[\[Minecraft の公式サイト\]](https://www.minecraft.net/ja-jp)からダウンロード・インストールしたとしても、アップデートのリストから除外せずにソフトウェアの更新を行ってしまうことで真の最新版から旧版（自称最新版）にアップデート（ダウングレード）されてしまいます、かなり罠。


ということで今回は真の最新版を維持するための方法のうち、一つを記事にしてみました。
能書はここまでにして早速行きましょう。

## Flatpak
[\[Flathub に存在する\]](https://flathub.org/apps/details/com.mojang.Minecraft) 非公式 (not verified by, affiliated with, or supported) の [\[wrapper\]](https://wa3.i-3-i.info/word191.html) を利用するのが一番楽です。
インストール方法は以下の通り。

```sh
flatpak install flathub com.mojang.Minecraft
```

これでインストールは完了し、メニューにも登録されます。
が、Flatpak の仕様上ゲームデータの保管場所が変わっているので [\[こちらの記事\]](https://www.f-matano44.jp/post/20220215_flatpak/) も是非参考にしてください。
